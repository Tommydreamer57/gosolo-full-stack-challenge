/*
    This is just a small sample to get you started. 
    Note that the docker setup will be looking for `index.js`,
    so it's best to use this file or the same file name.
 */
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const _ = require('lodash');

const SORTS = {
  name: ({ name }) => name,
  height: ({ height }) => +height,
  mass: ({ mass }) => +mass,
};

const fetchSwapiData = async (url, people = []) => {

  const { data: { next, results = [] } } = await axios.get(url);

  if (next) return fetchSwapiData(next, [...people, ...results]);
  else return [...people, ...results];
};

// const swapiCache = {
//   peoplePromise: fetchSwapiData('https://swapi.dev/api/people/'),
//   planetsPromise: fetchSwapiData('https://swapi.dev/api/planets/'),

//   getPeople: async () => await this.peoplePromise,
//   getPlanets: async () => await this.planetsPromise,
// };

const fetchPeople = () => fetchSwapiData('https://swapi.dev/api/people/');

const fetchPlanets = async () => {
  const people = await fetchSwapiData('https://swapi.dev/api/people/');
  const planets = await fetchSwapiData('https://swapi.dev/api/planets/');

  const peopleByUrl = people.reduce((all, person) => ({
    ...all,
    [person.url]: person,
  }), {});

  return planets.map(planet => ({
    ...planet,
    residents: planet.residents.map(residentUrl => peopleByUrl[residentUrl]?.name),
  }));
};

const PORT = process.env.PORT || 4000;
const app = express();

app.use(cors());

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/people', async (req, res) => {

  try {

    const people = await fetchPeople();

    const {
      query: {
        sortBy,
      },
    } = req;

    if (SORTS[sortBy]) {
      const sortedPeople = _.sortBy(people, [SORTS[sortBy]]);

      res.status(200).send(sortedPeople);
    }

    else res.status(200).send(people);

  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

app.get('/people/:id', async (req, res) => {
  try {
    const { params: { id } } = req;

    const { data } = await axios.get(`https://swapi.dev/api/people/${id}/`);

    console.log({
      id,
      data,
    });

    res.status(200).send(data);

  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

app.get('/planets', async (req, res) => {
  try {

    const planets = await fetchPlanets();

    res.status(200).send(planets);

  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
