import './App.css';
import SelectionProvider from './contexts/SelectionContext';
import SwapiProvider from './contexts/SwapiContext';
import SwapiCard from './views/SwapiCard';

export default function App() {

  return (
    <SwapiProvider>
      <SelectionProvider>
        <SwapiCard />
      </SelectionProvider>
    </SwapiProvider>
  );
}
