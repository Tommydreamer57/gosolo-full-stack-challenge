import ItemInfo from "../components/ItemInfo";
import PeoplePlanetsTable from "../components/PeoplePlanetsTable";
import PeoplePlanetsToggle from "../components/PeoplePlanetsToggle";
import { useSwapiContext } from "../contexts/SwapiContext";

export default function SwapiCard() {

  const { loading } = useSwapiContext();

  return (
    <div className="SwapiCard">
      <h1>
        Star Wars API
      </h1>
      <div className="flex-wrapper" >
        <PeoplePlanetsToggle />
        <div className="info-title" >
          Info
        </div>
      </div>
      {loading ? (
        <div>
          Loading data...
        </div>
      ) : (
          <div className="flex-wrapper" >
            <PeoplePlanetsTable />
            <ItemInfo />
          </div>
        )}
    </div>
  );
}
