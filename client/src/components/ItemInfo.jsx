import { useSelectionContext } from "../contexts/SelectionContext";

export default function ItemInfo() {

  const {
    displayPeople,
    person: selectedPerson,
    person: {
      name: personName,
      height: personHeight,
      hair_color,
      skin_color,
      eye_color,
      gender,
    } = {},
    planet: selectedPlanet,
    planet: {
      name: planetName,
      climate,
      gravity,
      terrain,
      population,
    } = {},
  } = useSelectionContext();

  const displayPerson = displayPeople && !!selectedPerson;
  const displayPlanet = !displayPeople && !!selectedPlanet;

  const data = displayPerson ?
    [
      ['Name', personName],
      ['Height', personHeight],
      ['Hair Color', hair_color],
      ['Skin Color', skin_color],
      ['Eye Color', eye_color],
      ['Gender', gender],
    ]
    :
    displayPlanet ?
      [
        ['Name', planetName],
        ['Climate', climate],
        ['Gravity', gravity],
        ['Terrain', terrain],
        ['Population', population],
      ]
      :
      [];

  return (
    <section className="info-wrapper" >
      {displayPerson || displayPlanet ? (
        <ul>
          {data.map(([label, value]) => (
            <li className="info-item" >
              {label}: {value}
            </li>
          ))}
        </ul>
      ) : (
          <span className="info-message" >
            Select a {displayPeople ? 'person' : 'planet'} from the list
          </span>
        )}
    </section>
  );
}
