import { useSelectionContext } from "../contexts/SelectionContext";
import { useSwapiContext } from "../contexts/SwapiContext";

export default function PeoplePlanetsTable() {

  const {
    displayPeople,
    person: selectedPerson,
    planet: selectedPlanet,
    selectPerson,
    selectPlanet,
  } = useSelectionContext();

  const {
    people,
    planets,
  } = useSwapiContext();

  const data = displayPeople ? people : planets;
  const selectedItem = displayPeople ? selectedPerson : selectedPlanet;
  const selectItem = displayPeople ? selectPerson : selectPlanet;

  const rows = data
    .map(item => ({ item }))
    .map(({
      item,
      item: {
        name,
        films: {
          length: filmCount = 0,
        } = [],
      },
    }) => ({
      item,
      name,
      filmCount,
      selected: item === selectedItem,
    }));

  return (
    <section className="table-wrapper">
      <table>
        <tr>
          <th>Name</th>
          <th># of Films</th>
        </tr>
        {rows.map(({ item, name, filmCount, selected }) => (
          <tr
            onClick={() => selectItem(item)}
            className={selected ? 'selected' : ''}
          >
            <td>
              {name}
            </td>
            <td>
              {filmCount}
            </td>
          </tr>
        ))}
      </table>
    </section>
  );
}
