import { useSelectionContext } from '../contexts/SelectionContext';

export default function PeoplePlanetsToggle() {

  const { displayPeople, setDisplayPeople, selectPerson, selectPlanet } = useSelectionContext();

  return (
    <div className="button-group">
      <button
        className={displayPeople ? '' : 'selected'}
        onClick={() => {
          setDisplayPeople(false);
          selectPerson();
        }}
      >
        Planets
      </button>
      <button
        className={displayPeople ? 'selected' : ''}
        onClick={() => {
          setDisplayPeople(true);
          selectPlanet();
        }}
      >
        People
      </button>
    </div>
  );
}
