import axios from 'axios';
import { createContext, useContext, useEffect, useState } from 'react';

const SwapiContext = createContext({
  people: [],
  planets: [],
  loading: false,
});

export const useSwapiContext = () => useContext(SwapiContext);

export default function SwapiProvider({ children }) {

  const [people, setPeople] = useState([]);
  const [planets, setPlanets] = useState([]);

  const [loadingPeople, setLoadingPeople] = useState(false);
  const [loadingPlanets, setLoadingPlanets] = useState(false);

  useEffect(() => {
    setLoadingPeople(true);
    setLoadingPlanets(true);
    axios.get('http://localhost:4000/people').then(({ data }) => {
      setPeople(data);
      setLoadingPeople(false);
    });
    axios.get('http://localhost:4000/planets').then(({ data }) => {
      setPlanets(data);
      setLoadingPlanets(false);
    });
  }, []);

  console.log({
    people,
    planets,
    loadingPeople,
    loadingPlanets,
  });

  return (
    <SwapiContext.Provider
      value={{
        people,
        planets,
        loading: loadingPeople || loadingPlanets,
      }}
    >
      {children}
    </SwapiContext.Provider>
  );
}
