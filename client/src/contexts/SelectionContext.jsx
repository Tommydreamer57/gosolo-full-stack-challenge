import { createContext, useContext, useState } from 'react';

const SelectionContext = createContext({
  displayPeople: true,
  person: undefined,
  planet: undefined,
  setDisplayPeople: () => { },
  selectPerson: () => { },
  selectPlanet: () => { },
});

export const useSelectionContext = () => useContext(SelectionContext);

export default function SelectionProvider({ children }) {

  const [displayPeople, setDisplayPeople] = useState(true);

  const [person, selectPerson] = useState();
  const [planet, selectPlanet] = useState();

  return (
    <SelectionContext.Provider
      value={{
        displayPeople,
        person,
        planet,
        setDisplayPeople,
        selectPerson,
        selectPlanet,
      }}
    >
      {children}
    </SelectionContext.Provider>
  );
}
